var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

const _ = require('lodash')

var accounts_JSON = require('./account.json');
var card_JSON = require('./card.json');
var goal_JSON = require('./goal.json');
var movement_JSON = require('./movement.json');
var personnel_JSON = require('./personnel.json');
var user_JSON = require('./user.json');

var bodyparser = require('body-parser');
const { Console } = require('console');
//app.options('*', cors());
app.use(bodyparser.json());

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});
app.post('/', (req, res) => {
  res.send('Hola hemos recibido su peticion post');
});
app.put('/', (req, res) => {
  res.send('Hola hemos recibido su peticion put');
});
app.delete('/', (req, res) => {
  res.send('Hola hemos recibido su peticion delete cambiada');
});

app.get('/clientes/:idcliente', (req, res) => {
  res.send('Aqui tiene al cliente: ' + req.params.idcliente);
});

app.get('/v2/Movements', (req, resp) => {
  resp.json(movement_JSON);
});

app.get('/v2/Movementsquery', function (req, res) {
  res.send('Recibido: ' + req.query.nombre);
})

app.post('/v2/Movements', (req, resp) => {
  let nuevo = req.body;
  nuevo.id = movement_JSON.length + 1;
  movement_JSON.push(nuevo);
  resp.send('Movimiento dado de alta con id: ' + nuevo.id);
})

//Login SERVICE
app.post('/v2/login', (req, resp) => {
  var identifier = req.body.indentifier;
  var password = req.body.pswd;
  var user = _.find(user_JSON, (element) => {
    return element.email === identifier && element.psw === password;
  });
  if (!user) {
    user = _.find(user_JSON, (element) => {
      return element.user_code === identifier && element.psw === password;
    });
  }
  if (!user) {
    user = { "status": "fail" }
  }
  resp.json(user);
});

//MOVIMIENTOS
app.get('/v2/Movements/:personnel_id', (req, resp) => {
  var accountF = _.filter(accounts_JSON, (element) => {
    return element.personnel_id == req.params.personnel_id
  });
  var movements = [];
  _.forEach(accountF, (element) => {
    _.forEach(movement_JSON, (move) => {
      if (element.account_id == move.account_id) {
        move["account_code"] = "****" + element.account_code.substring(element.account_code.length - 4);
        movements.push(move);
      }
    });
  });
  var sortMovemts = _.orderBy(movements, [
    (object) => {
      return new Date(object.date);
    }], ["desc"]);
  resp.json(sortMovemts);
});

//PREDICTION
app.get('/v2/Prediction/:personnel_id', (req, resp) => {
  var accountF = _.filter(accounts_JSON, (element) => {
    return element.personnel_id.toString() === req.params.personnel_id.toString()
  });
  var movements = [];
  _.forEach(accountF, (element) => {
    _.forEach(movement_JSON, (move) => {
      if (element.account_id === move.account_id) {
        movements.push(move);
      }
    });
  });
  var sortMovemts = _.orderBy(movements, [
    (object) => {
      return new Date(object.date);
    }], ["desc"]);
  var aux = [];
  sortMovemts.forEach(element => {
    if (!aux[element.date]) {
      aux[element.date] = element.quantity;
    } else {
      aux[element.date] = aux[element.date] + element.quantity;
    }
  });
  var lastDate = sortMovemts[sortMovemts.length - 1].date;
  var tempBalance = [];
  var dates = [];
  var d = new Date();
  var tempInputs = 0;
  var tempOutputs = 0;
  var cont = true;
  var fDate = formatDate(d);
  var month = fDate.substring(0, fDate.length - 3);
  var prevDate = month;
  while (cont) {
    if(dates.indexOf(month) == -1){
      dates.push(month);
    }
    if (prevDate != month) {
      tempBalance.push(tempInputs - tempOutputs);
      tempInputs = 0;
      tempOutputs = 0;
      prevDate = month;
    }
    if (!!aux[fDate]) {
      if (aux[fDate] > 0) {
        tempInputs += aux[fDate];
      } else {
        tempOutputs += aux[fDate];
      }
    }
    d = new Date(d.setDate(d.getDate() - 1));
    fDate = formatDate(d);
    month = fDate.substring(0, fDate.length - 3);
    if (lastDate === fDate) {
      cont = false;
      tempBalance.push(tempInputs - tempOutputs);
    }
  }
  var sum = _.sum(tempBalance);
  var prom = sum/tempBalance.length;
  var goalsF = _.filter(goal_JSON, (element) => {
    return element.personnel_id.toString() === req.params.personnel_id.toString()
  });
  var milstone = 0;
  _.forEach(goalsF, (element) => {
    milstone = milstone + element.milestone;
  });
  var numM = milstone/prom;
  var auxDate = new Date(lastDate);
  var dates = [];
  var balanceProm = [];
  for(var i = 0; i <= numM+1; i++ ){
    var fomatDate = formatDate(auxDate);
    var month = fomatDate.substring(0, fomatDate.length - 3);
    dates.push(month);
    auxDate = new Date(auxDate.setMonth(auxDate.getMonth()+1));
    balanceProm.push(prom*i+1);
  }
  var respObject = {"data":balanceProm, "cat":dates, "goalTotal":milstone, "average": prom, "goals": goalsF}
  resp.json(respObject);
});

//PERSONNEL AND USER SERVICES
app.get('/v2/Users/:id', (req, resp) => {
  var userFind = personnel_JSON.find(element => {
    return element.id == req.params.id;
  });
  resp.json(userFind);
});

//ACOUNT SERVICE
app.get('/v2/Accounts/:person_id', (req, resp) => {
  resp.send(accounts_JSON[req.params.id]);
});

app.get('/v2/Accounts/balance/:personnel_id', (req, resp) => {
  var accountF = _.filter(accounts_JSON, (element) => {
    return element.personnel_id == req.params.personnel_id
  });
  var balance = 0;
  _.forEach(accountF, (element) => {
    balance += element.balance;
  });
  var personObj = { "balance": balance }
  resp.json(personObj);
});

//GOAL SERVICE
app.get('/v2/Goals', (req, resp) => {
  var objsFiltered = _.filter(goal_JSON, (element) => {
    return element.personnel_id == req.query.personnel_id
  });
  resp.json(objsFiltered);
});

//Financial full. Recibe personn_id return only finance data
app.get('/v2/finxstats/:personnel_id', (req, resp) => {
  var goalsF = _.filter(goal_JSON, (element) => {
    return element.personnel_id == req.params.personnel_id
  });
  var accountF = _.filter(accounts_JSON, (element) => {
    return element.personnel_id == req.params.personnel_id
  });

  var movsF = [];
  _.forEach(accountF, (element) => {
    _.forEach(movement_JSON, (move) => {
      if (element.account_id == move.account_id) {
        movsF.push(move);
      }
    });
  });
  var inputs = 0;
  var outputs = 0;
  _.forEach(movsF, (element) => {
    if (element.quantity < 0) {
      outputs = outputs - element.quantity;
    } else {
      inputs = inputs + element.quantity;
    }
  });
  var totalBalance = 0;
  _.forEach(accountF, (element) => {
    totalBalance = totalBalance + element.balance;
  });
  var milstone = 0;
  _.forEach(goalsF, (element) => {
    milstone = milstone + element.milestone;
  });
  var finData = { balance: totalBalance, goalN: milstone, goalP: (totalBalance / (milstone / 100)).toFixed(0), input: inputs, output: outputs, goals: goalsF.length }
  resp.json(finData);
});

function formatDate(date) {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();
  if (month.length < 2)
    month = '0' + month;
  if (day.length < 2)
    day = '0' + day;
  return [year, month, day].join('-');
}
